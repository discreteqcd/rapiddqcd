
import math as m
import sys
from random import random

from primitiveLink import PrimitiveLink
from qcd import deltaYg, deltaYq
from basics import eq, gt, lt, le

class Primitive:

    def __init__(self, name, yMin, yMax, kappaMin, kappaMax):

        print ("initialize primitive ", name)
        # identifiers
        self.name = name
        self.prob=0.0
        # kinematical properties (to e.g. calculate correct weight)
        self.kappaMin = kappaMin
        self.kappaMax = kappaMax
        self.y0 = 0.0
        self.yMin = yMin
        self.yMax = yMax
        self.weight = 1.
        self.kappaOfHighestBkgrndTile = m.floor((self.kappaMax - 2.*deltaYq/2.)/(2.*deltaYg)) * 2.*deltaYg 

        # set up the primitive grove structure
        self.nodes = self.createLinks(name)
        # derived properties
        self.split_systems = []
        self.setup()
        self.weight = self.calcWeights()

        self.smearing_weight = 1.

        self.kappaMinSecondary = -1.

        return None

    def set_kappa_min_secondary(self, kappa_in):
        self.kappaMinSecondary = kappa_in
        if le(self.kappaMinSecondary, 0.):
          self.kappaMinSecondary = self.kappaMax-deltaYq

    def calc_kappa_min_secondary(self):
        kappaMinSecondary = -1.
        max_height = 2
        ybin = 1.0
        kMax=self.kappaMax-deltaYq
        kMin=max(kMax-2.*deltaYg,0.0)

        area = self.calcPhasespaceArea(ybin, 2.*deltaYg , deltaYg, kMax)
        if area < 1e-3:
          self.set_kappa_min_secondary(-1.0)
          return -1.0
        area = self.calcPhasespaceArea(ybin, 2.*deltaYg , deltaYg, kMin)
        if area > 1e-3:
          self.set_kappa_min_secondary(kMin)
          return kMin
        for i in range(1,101):
          hf = float(i)/100.
          know=hf*kMax
          area = self.calcPhasespaceArea(ybin, 2.*deltaYg , deltaYg, know)
          if gt(area,0.):
            kappaMinSecondary = know
            break

        self.set_kappa_min_secondary(kappaMinSecondary)
        return self.kappaMinSecondary


    def list(self):
        for n in self.nodes:
          n.list()

    def event_weight(self):
        return self.weight*self.smearing_weight

    def createLinks(self,name):

        nodes = []
        if name=="A":
          nodes.append(PrimitiveLink(1,1,2, True, False,  -1.5))
          nodes.append(PrimitiveLink(1,2,3, False, False, -0.5))
          nodes.append(PrimitiveLink(1,3,4, False, False,  0.5))
          nodes.append(PrimitiveLink(1,4,5, False, True,   1.5))
          self.prob = 1./12.
        elif name=="B":
          nodes.append(PrimitiveLink(1,1,2, True, False , -1.5 ))
          nodes.append(PrimitiveLink(2*100,2,3, False, True, -1.0))
          nodes.append(PrimitiveLink(2*100,3,4, True, False, -1.0))
          nodes.append(PrimitiveLink(1,4,5, False, False, -0.5))
          nodes.append(PrimitiveLink(1,5,6, False, False, 0.5))
          nodes.append(PrimitiveLink(1,6,7, False, True, 1.5))
          self.prob = 1./12.
        elif name=="C":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(1,2,3, False, False, -0.5))
          nodes.append(PrimitiveLink(3*100,3,4, False, True, 0.0))
          nodes.append(PrimitiveLink(3*100,4,5, True, False, 0.0))
          nodes.append(PrimitiveLink(1,5,6, False, False, 0.5))
          nodes.append(PrimitiveLink(1,6,7, False, True, 1.5))
          self.prob = 1./12.
        elif name=="D":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(1,2,3, False, False, -0.5))
          nodes.append(PrimitiveLink(1,3,4, False, False, 0.5))
          nodes.append(PrimitiveLink(4*100,4,5, False, True, 1.))
          nodes.append(PrimitiveLink(4*100,5,6, True, False, 1.))
          nodes.append(PrimitiveLink(1,6,7, False, True, 1.5))
          self.prob = 1./12.
        elif name=="E":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(1,2,3, False, False, -0.5))
          nodes.append(PrimitiveLink(3*100,3,4, False, False, 0.))
          nodes.append(PrimitiveLink(3*100,4,5, False, True, 0.))
          nodes.append(PrimitiveLink(3*100,5,6, True, False, 0.))
          nodes.append(PrimitiveLink(3*100,6,7, False, False, 0.))
          nodes.append(PrimitiveLink(1,7,8, False, False, 0.5))
          nodes.append(PrimitiveLink(1,8,9, False, True, 1.5))
          self.prob = 1./12. * 1./2. * 1./2.
        elif name=="F":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(2*100,2,3, False, True, -1.))
          nodes.append(PrimitiveLink(2*100,3,4, True, False, -1.))
          nodes.append(PrimitiveLink(1,4,5, False, False, -0.5))
          nodes.append(PrimitiveLink(5*100,5,6, False, True, 0.))
          nodes.append(PrimitiveLink(5*100,6,7, True, False,0.))
          nodes.append(PrimitiveLink(1,7,8, False, False, 0.5))
          nodes.append(PrimitiveLink(1,8,9, False, True, 1.5))
          self.prob = 1./12.
        elif name=="G":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(2*100,2,3, False, True, -1.))
          nodes.append(PrimitiveLink(2*100,3,4, True, False, -1.))
          nodes.append(PrimitiveLink(1,4,5, False, False, -0.5))
          nodes.append(PrimitiveLink(1,5,6, False, False, 0.5))
          nodes.append(PrimitiveLink(6*100,6,7, False, True, 1.))
          nodes.append(PrimitiveLink(6*100,7,8, True, False, 1.))
          nodes.append(PrimitiveLink(1,8,9, False, True, 1.5))
          self.prob = 1./12.
        elif name=="H":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(1,2,3, False, False, -0.5))
          nodes.append(PrimitiveLink(3*100,3,4, False, True, 0.))
          nodes.append(PrimitiveLink(3*100,4,5, True, False, 0.))
          nodes.append(PrimitiveLink(1,5,6, False, False, 0.5))
          nodes.append(PrimitiveLink(6*100,6,7, False, True, 1.))
          nodes.append(PrimitiveLink(6*100,7,8, True, False, 1.))
          nodes.append(PrimitiveLink(1,8,9, False, True, 1.5))
          self.prob = 1./12.
        elif name=="I":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(2*100,2,3, False, True, -1.))
          nodes.append(PrimitiveLink(2*100,3,4, True, False, -1.))
          nodes.append(PrimitiveLink(1,4,5, False, False, -0.5))
          nodes.append(PrimitiveLink(5*100,5,6, False, False, 0.))
          nodes.append(PrimitiveLink(5*100,6,7, False, True, 0.))
          nodes.append(PrimitiveLink(5*100,7,8, True, False, 0.))
          nodes.append(PrimitiveLink(5*100,8,9, False, False, 0.))
          nodes.append(PrimitiveLink(1,9,10, False, False, 0.5))
          nodes.append(PrimitiveLink(1,10,11, False, True, 1.5))
          self.prob = 1./12. * 1./2. * 1./2.
        elif name=="J":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(1,2,3, False, False, -0.5))
          nodes.append(PrimitiveLink(3*100,3,4, False, False, 0.))
          nodes.append(PrimitiveLink(3*100,4,5, False, True, 0.))
          nodes.append(PrimitiveLink(3*100,5,6, True, False, 0.))
          nodes.append(PrimitiveLink(3*100,6,7, False, False, 0.))
          nodes.append(PrimitiveLink(1,7,8, False, False, -0.5))
          nodes.append(PrimitiveLink(8*100,8,9, False, True, 1.))
          nodes.append(PrimitiveLink(8*100,9,10, True, False, 1.))
          nodes.append(PrimitiveLink(1,10,11, False, True, 1.5))
          self.prob = 1./12. * 1./2. * 1./2.
        elif name=="K":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(1,2,3, False, False, -0.5))
          nodes.append(PrimitiveLink(3*100,3,4, False, False, 0.))
          nodes.append(PrimitiveLink(3*100+4*10000,4,5, False, True, -1.))
          nodes.append(PrimitiveLink(3*100+4*10000,5,6, True, False, -1.))
          nodes.append(PrimitiveLink(3*100,6,7, False, True, 0.))
          nodes.append(PrimitiveLink(3*100,7,8, True, False, 0.))
          nodes.append(PrimitiveLink(3*100,8,9, False, False, 0.))
          nodes.append(PrimitiveLink(1,9,10, False, False, 0.5))
          nodes.append(PrimitiveLink(1,10,11, False, True, 1.5))
          self.prob = 1./12. * 1./2. * 1./2.
        elif name=="L":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(1,2,3, False, False, -0.5))
          nodes.append(PrimitiveLink(3*100,3,4, False, False, 0.))
          nodes.append(PrimitiveLink(3*100,4,5, False, True, 0.))
          nodes.append(PrimitiveLink(3*100,5,6, True, False, 0.))
          nodes.append(PrimitiveLink(3*100+6*10000,6,7, False, True, 1.))
          nodes.append(PrimitiveLink(3*100+6*10000,7,8, True, False, 1.))
          nodes.append(PrimitiveLink(3*100,8,9, False, False, 0.))
          nodes.append(PrimitiveLink(1,9,10, False, False, 0.5))
          nodes.append(PrimitiveLink(1,10,11, False, True, 1.5))
          self.prob = 1./12. * 1./2. * 1./2.
        elif name=="M":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(2*100,2,3, False, True, -1.))
          nodes.append(PrimitiveLink(2*100,3,4, True, False, -1.))
          nodes.append(PrimitiveLink(1,4,5, False, False, -0.5))
          nodes.append(PrimitiveLink(5*100,5,6, False, True, 0.))
          nodes.append(PrimitiveLink(5*100,6,7, True, False, 0.))
          nodes.append(PrimitiveLink(1,7,8, False, False, 0.5))
          nodes.append(PrimitiveLink(8*100,8,9, False, True, 1.))
          nodes.append(PrimitiveLink(8*100,9,10, True, False, 1.))
          nodes.append(PrimitiveLink(1,10,11, False, True, 1.5))
          self.prob = 1./12.
        elif name=="N":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(2*100,2,3, False, True, -1.))
          nodes.append(PrimitiveLink(2*100,3,4, True, False, -1.))
          nodes.append(PrimitiveLink(1,4,5, False, False, -0.5))
          nodes.append(PrimitiveLink(5*100,5,6, False, False, 0.))
          nodes.append(PrimitiveLink(5*100,6,7, False, True, 0.))
          nodes.append(PrimitiveLink(5*100,7,8, True, False, 0.))
          nodes.append(PrimitiveLink(5*100,8,9, False, False, 0.))
          nodes.append(PrimitiveLink(1,9,10, False, False, 0.5))
          nodes.append(PrimitiveLink(10*100,10,11, False, True, 1.))
          nodes.append(PrimitiveLink(10*100,11,12, True, False, 1.))
          nodes.append(PrimitiveLink(1,12,13, False, True, 1.5))
          self.prob = 1./12. * 1./2. * 1./2.
        elif name=="O":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(2*100,2,3, False, True, -1.))
          nodes.append(PrimitiveLink(2*100,3,4, True, False, -1.))
          nodes.append(PrimitiveLink(1,4,5, False, False, -0.5))
          nodes.append(PrimitiveLink(5*100,5,6, False, False, 0.))
          nodes.append(PrimitiveLink(5*100+6*10000,6,7, False, True, -1.))
          nodes.append(PrimitiveLink(5*100+6*10000,7,8, True, False, -1.))
          nodes.append(PrimitiveLink(5*100,8,9, False, True, 0.))
          nodes.append(PrimitiveLink(5*100,9,10, True, False, 0.))
          nodes.append(PrimitiveLink(5*100,10,11, False, False, 0.))
          nodes.append(PrimitiveLink(1,11,12, False, False, 0.5))
          nodes.append(PrimitiveLink(1,12,13, False, True, 1.5))
          self.prob = 1./12. * 1./2. * 1./2.
        elif name=="P":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(1,2,3, False, False, -0.5))
          nodes.append(PrimitiveLink(3*100,3,4, False, False, 0.))
          nodes.append(PrimitiveLink(3*100+4*10000,4,5, False, True, -1.))
          nodes.append(PrimitiveLink(3*100+4*10000,5,6, True, False, -1.))
          nodes.append(PrimitiveLink(3*100,6,7, False, True, 0.))
          nodes.append(PrimitiveLink(3*100,7,8, True, False, 0.))
          nodes.append(PrimitiveLink(3*100,8,9, False, False, 0.))
          nodes.append(PrimitiveLink(1,9,10, False, False, 0.5))
          nodes.append(PrimitiveLink(10*100,10,11, False, True, 1.))
          nodes.append(PrimitiveLink(10*100,11,12, True, False, 1.))
          nodes.append(PrimitiveLink(1,12,13, False, True, 1.5))
          self.prob = 1./12. * 1./2. * 1./2.
        elif name=="Q":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(2*100,2,3, False, True, -1.))
          nodes.append(PrimitiveLink(2*100,3,4, True, False, -1.))
          nodes.append(PrimitiveLink(1,4,5, False, False, -0.5))
          nodes.append(PrimitiveLink(5*100,5,6, False, False, 0.))
          nodes.append(PrimitiveLink(5*100,6,7, False, True, 0.))
          nodes.append(PrimitiveLink(5*100,7,8, True, False, 0.))
          nodes.append(PrimitiveLink(5*100+8*10000,8,9, False, True, 1.))
          nodes.append(PrimitiveLink(5*100+8*10000,9,10, True, False, 1.))
          nodes.append(PrimitiveLink(5*100,10,11, False, False, 0.))
          nodes.append(PrimitiveLink(1,11,12, False, False, 0.5))
          nodes.append(PrimitiveLink(1,12,13, False, True, 1.5))
          self.prob = 1./12. * 1./2. * 1./2.
        elif name=="R":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(1,2,3, False, False, -0.5))
          nodes.append(PrimitiveLink(3*100,3,4, False, False, 0.))
          nodes.append(PrimitiveLink(3*100,4,5, False, True, 0.))
          nodes.append(PrimitiveLink(3*100,5,6, True, False, 0.))
          nodes.append(PrimitiveLink(3*100+6*10000,6,7, False, True, 1.))
          nodes.append(PrimitiveLink(3*100+6*10000,7,8, True, False, 1.))
          nodes.append(PrimitiveLink(3*100,8,9, False, False, 0.))
          nodes.append(PrimitiveLink(1,9,10, False, False, 0.5))
          nodes.append(PrimitiveLink(10*100,10,11, False, True, 1.))
          nodes.append(PrimitiveLink(10*100,11,12, True, False, 1.))
          nodes.append(PrimitiveLink(1,12,13, False, True, 1.5))
          self.prob = 1./12. * 1./2. * 1./2.
        elif name=="S":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(1,2,3, False, False, -0.5))
          nodes.append(PrimitiveLink(3*100,3,4, False, False, 0.))
          nodes.append(PrimitiveLink(3*100+4*10000,4,5, False, True, -1.))
          nodes.append(PrimitiveLink(3*100+4*10000,5,6, True, False, -1.))
          nodes.append(PrimitiveLink(3*100,6,7, False, True, 0.))
          nodes.append(PrimitiveLink(3*100,7,8, True, False, 0.))
          nodes.append(PrimitiveLink(3*100+8*10000,8,9, False, True, 1.))
          nodes.append(PrimitiveLink(3*100+8*10000,9,10, True, False, 1.))
          nodes.append(PrimitiveLink(3*100,10,11, False, False, 0.))
          nodes.append(PrimitiveLink(1,11,12, False, False, 0.5))
          nodes.append(PrimitiveLink(1,12,13, False, True, 1.5))
          self.prob = 1./12. * 1./2. * 1./2.
        elif name=="T":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(2*100,2,3, False, True, -1.))
          nodes.append(PrimitiveLink(2*100,3,4, True, False, -1.))
          nodes.append(PrimitiveLink(1,4,5, False, False, -0.5))
          nodes.append(PrimitiveLink(5*100,5,6, False, False, 0.))
          nodes.append(PrimitiveLink(5*100+6*10000,6,7, False, True, -1.))
          nodes.append(PrimitiveLink(5*100+6*10000,7,8, True, False, -1.))
          nodes.append(PrimitiveLink(5*100,8,9, False, True, 0.))
          nodes.append(PrimitiveLink(5*100,9,10, True, False, 0.))
          nodes.append(PrimitiveLink(5*100+10*10000,10,11, False, True, 1.))
          nodes.append(PrimitiveLink(5*100+10*10000,11,12, True, False, 1.))
          nodes.append(PrimitiveLink(5*100,12,13, False, False, 0.))
          nodes.append(PrimitiveLink(1,13,14, False, False, 0.5))
          nodes.append(PrimitiveLink(1,14,15, False, True, 1.5))
          self.prob = 1./12. * 1./2. * 1./2.
        elif name=="U":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(1,2,3, False, False, -0.5))
          nodes.append(PrimitiveLink(3*100,3,4, False, False, 0.))
          nodes.append(PrimitiveLink(3*100+4*10000,4,5, False, True, -1.))
          nodes.append(PrimitiveLink(3*100+4*10000,5,6, True, False, -1.))
          nodes.append(PrimitiveLink(3*100,6,7, False, True, 0.))
          nodes.append(PrimitiveLink(3*100,7,8, True, False, 0.))
          nodes.append(PrimitiveLink(3*100+8*10000,8,9, False, True, 1.))
          nodes.append(PrimitiveLink(3*100+8*10000,9,10, True, False, 1.))
          nodes.append(PrimitiveLink(3*100,10,11, False, False, 0.))
          nodes.append(PrimitiveLink(1,11,12, False, False, 0.5))
          nodes.append(PrimitiveLink(12*100,12,13, False, True, 1.))
          nodes.append(PrimitiveLink(12*100,13,14, True, False, 1.))
          nodes.append(PrimitiveLink(1,14,15, False, True, 1.5))
          self.prob = 1./12. * 1./2. * 1./2.
        elif name=="V":
          nodes.append(PrimitiveLink(1,1,2, True, False,-1.5))
          nodes.append(PrimitiveLink(2*100,2,3, False, True, -1.))
          nodes.append(PrimitiveLink(2*100,3,4, True, False, -1.))
          nodes.append(PrimitiveLink(1,4,5, False, False, 0.5))
          nodes.append(PrimitiveLink(5*100,5,6, False, False, 0.))
          nodes.append(PrimitiveLink(5*100+6*10000,6,7, False, True, -1.))
          nodes.append(PrimitiveLink(5*100+6*10000,7,8, True, False, -1.))
          nodes.append(PrimitiveLink(5*100,8,9, False, True, 0.))
          nodes.append(PrimitiveLink(5*100,9,10, True, False, 0.))
          nodes.append(PrimitiveLink(5*100,10,11, False, False, 0.))
          nodes.append(PrimitiveLink(1,11,12, False, False, 0.5))
          nodes.append(PrimitiveLink(12*100,12,13, False, True, 1.))
          nodes.append(PrimitiveLink(12*100,13,14, True, False, 1.))
          nodes.append(PrimitiveLink(1,14,15, False, True, 1.5))
          self.prob = 1./12. * 1./2. * 1./2.
        elif name=="W":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(2*100,2,3, False, True,-1.))
          nodes.append(PrimitiveLink(2*100,3,4, True, False,-1.))
          nodes.append(PrimitiveLink(1,4,5, False, False, -0.5))
          nodes.append(PrimitiveLink(5*100,5,6, False, False, 0.))
          nodes.append(PrimitiveLink(5*100,6,7, False, True, 0.))
          nodes.append(PrimitiveLink(5*100,7,8, True, False, 0.))
          nodes.append(PrimitiveLink(5*100+8*10000,8,9, False, True, 1.))
          nodes.append(PrimitiveLink(5*100+8*10000,9,10, True, False, 1.))
          nodes.append(PrimitiveLink(5*100,10,11, False, False, 0.))
          nodes.append(PrimitiveLink(1,11,12, False, False, 0.5))
          nodes.append(PrimitiveLink(12*100,12,13, False, True, 1.))
          nodes.append(PrimitiveLink(12*100,13,14, True, False, 1.))
          nodes.append(PrimitiveLink(1,14,15, False, True, 1.5))
          self.prob = 1./12. * 1./2. * 1./2.
        elif name=="X":
          nodes.append(PrimitiveLink(1,1,2, True, False, -1.5))
          nodes.append(PrimitiveLink(2*100,2,3, False, True, -1.))
          nodes.append(PrimitiveLink(2*100,3,4, True, False, -1.))
          nodes.append(PrimitiveLink(1,4,5, False, False, -0.5))
          nodes.append(PrimitiveLink(5*100,5,6, False, False, 0.))
          nodes.append(PrimitiveLink(5*100+6*10000,6,7, False, True, -1.))
          nodes.append(PrimitiveLink(5*100+6*10000,7,8, True, False, -1.))
          nodes.append(PrimitiveLink(5*100,8,9, False, True, 0.))
          nodes.append(PrimitiveLink(5*100,9,10, True, False, 0.))
          nodes.append(PrimitiveLink(5*100+10*10000,10,11, False, True, 1.))
          nodes.append(PrimitiveLink(5*100+10*10000,11,12, True, False, 1.))
          nodes.append(PrimitiveLink(5*100,12,13, False, False, 0.))
          nodes.append(PrimitiveLink(1,13,14, False, False, 0.5))
          nodes.append(PrimitiveLink(14*100,14,15, False, True, 1.))
          nodes.append(PrimitiveLink(14*100,15,16, True, False, 1.))
          nodes.append(PrimitiveLink(1,16,17, False, True, 1.5))
          self.prob = 1./12. * 1./2. * 1./2.
        else:
          print ("Unknown structure name", name, ". Name has to be in [A...X]")

        return nodes

    def calcWeights(self):

        weight=1.

        for i_n in range(len(self.nodes)):
          n = self.nodes[i_n]
          if n.lineColor > 100 and n.startIsGluon:
            continue
          if n.lineColor > 100 and not n.stopIsGluon and (self.nodes[i_n+1].lineColor > 10000 or self.nodes[i_n-1].lineColor > 10000):
            continue
          if n.lineColor < 100 and i_n+1 < len(self.nodes) and self.nodes[i_n+1].lineColor > 100:
            continue
          if eq(n.bkgrnd_ybin, 1.5):
            continue

          delta = self.calc_delta(n)
          ybin = self.calc_ybin(n)
          max_height = self.calc_maxKappaHeight(i_n,ybin)
          if max_height== 0:
            continue

          # the probability distribution is flat on the plane, i.e. the 
          # probability to pick one 'tile' is given by its area. Tiles at the
          # border of phase space have smaller area, i.e. lower probability,
          # i.e. lower weight
          sum_estimated_area = 0.
          sum_real_area = 0.;
          estimated_areas = []
          real_areas = []
          for h in range(0,max_height):
            area = self.calcPhasespaceArea(ybin, 2.*h*deltaYg , delta, n.kappaMax)
            sum_real_area += area
            sum_estimated_area += deltaYg * 2. * deltaYg
            real_areas.append(area)
            estimated_areas.append(deltaYg * 2. * deltaYg)
          real_prob = []
          estimated_prob = []
          for h in range(0,max_height):
            if gt(real_areas[h], 0.) and gt(estimated_areas[h],0.):
              real_prob.append( real_areas[h] / sum_real_area)
              estimated_prob.append( estimated_areas[h] / sum_estimated_area)
            else:
              real_prob.append( 1.)
              estimated_prob.append( 1.)

          node_height = self.calc_kappaHeight(i_n)
          weight *= real_prob[node_height]/estimated_prob[node_height]

        return weight


    def calc_delta(self,node):
        delta=deltaYg
        if node.lineColor < 10000:
          delta=deltaYq
        if node.lineColor>100 and node.lineColor < 10000 and node.startIsGluon == False and node.stopIsGluon==False:
          delta = deltaYg
        return delta


    def calc_ybin(self,node):
        ybin=1000.
        if node.lineColor > 10000 or (node.lineColor>100 and node.lineColor < 10000 and node.startIsGluon == False and node.stopIsGluon==False):
          ybin = 1.0
        elif node.lineColor < 100:
          if eq(node.bkgrnd_ybin, -1.5):
            ybin=-1.0
          elif eq(node.bkgrnd_ybin, -0.5):
            ybin=0.0
          elif eq(node.bkgrnd_ybin, 0.5):
            ybin=1.0
        else:
          ybin = node.bkgrnd_ybin

        return ybin

    def calc_kappaHeight(self,i_node):
        height = 0
        node = self.nodes[i_node]
        if node.lineColor > 100 and node.lineColor < 10000:
          height = 1
          max_height = self.calc_maxKappaHeight(i_node, node.bkgrnd_ybin)
          if max_height == 2:
            height = 1
          elif max_height == 3:
            mother_lineColor = self.nodes[i_node-1].lineColor
            daughter_lineColor = self.nodes[i_node+1].lineColor
            if mother_lineColor < 100 or daughter_lineColor < 100:
              height = 1
            else:
              height = 2
        if node.lineColor > 10000:
          height = 1
        if node.lineColor>100 and node.lineColor < 10000 and node.startIsGluon == False and node.stopIsGluon==False:
          height = 0

        return height


    def calc_maxKappaHeight(self,i_node,ybin):
        if abs(ybin)>1000.:
          return 0
        height = 0
        if self.nodes[i_node].lineColor < 10000:
          if eq(abs(ybin), 1.):
            height = 2
          elif eq(abs(ybin), 0.):
            height = 3
        elif self.nodes[i_node].lineColor > 10000:
          height = 2

        return height


    def calcPhasespaceArea(self, y_bin, kappaCenter, delta, kappaMax):

        yC = y_bin*deltaYg
        yL = (y_bin-0.5)*deltaYg
        yR = (y_bin+0.5)*deltaYg

        # Do not calculate the area if kappaMax signals to postpone calculation. 
        if kappaMax < 0.:
          return deltaYg * 2. * deltaYg

        # Set weight to zero if the maximal possible kappa (even before phase 
        # space checks) is below the cut-off
        kappaMaxSliceC = self.kappaMaxNow(yC, kappaMax)
        if le(kappaMaxSliceC, self.kappaMin):
          return 0.0

        kappaLowerEdge= kappaCenter-1.*deltaYg
        kappaUpperEdge= kappaCenter+1.*deltaYg
        # Now calculate the highest kappa value within the rapidity slice that
        # is allowed by phase space constraints.
        kappaMaxModSliceC = self.kappaMaxMod(yC, kappaMax, delta)
        kappaMaxModSliceL = self.kappaMaxMod(yL, kappaMax, delta)
        kappaMaxModSliceR = self.kappaMaxMod(yR, kappaMax, delta)

        if eq(kappaCenter,0.):
          return 0.5*deltaYg * 2. * deltaYg

        if lt(kappaUpperEdge, kappaMaxModSliceC) and lt(kappaUpperEdge, kappaMaxModSliceL) and lt(kappaUpperEdge, kappaMaxModSliceR):
          return deltaYg * 2. * deltaYg

        diff1 = abs(kappaMaxModSliceR - kappaMaxModSliceL)
        if eq(y_bin,0.):
          diff1 = kappaMaxModSliceC - kappaMaxModSliceL

        diff2 = 0.0
        maxKmodLR = max(kappaMaxModSliceR,kappaMaxModSliceL)
        if gt(maxKmodLR, kappaLowerEdge + 2.*deltaYg):
          diff2 = maxKmodLR - (kappaLowerEdge + 2.*deltaYg)

        minKmodLR = min(kappaMaxModSliceR,kappaMaxModSliceL)
        diff3 = ( kappaLowerEdge + 2.*deltaYg - minKmodLR )*deltaYg/diff1

        area2=0.0
        if eq(y_bin,0.):
          #diff   = kappaMaxModSliceC - kappaMaxModSliceL
          area2  = 0.5*deltaYg*(kappaMaxModSliceL-kappaLowerEdge)
          area2 += 0.5*0.5*deltaYg*diff1
          area2 *= 2.
        if lt(y_bin,0.):
          #diff   = kappaMaxModSliceR - kappaMaxModSliceL
          area2  = deltaYg*(kappaMaxModSliceL-kappaLowerEdge)
          area2 += 0.5*deltaYg*diff1
          area2 -= 0.5*diff3*diff2
        if gt(y_bin,0.):
          #diff   = kappaMaxModSliceL - kappaMaxModSliceR
          area2  = deltaYg*(kappaMaxModSliceR-kappaLowerEdge)
          area2 += 0.5*deltaYg*diff1
          area2 -= 0.5*diff3*diff2

        #area=0.0
        #if le(kappaLowerEdge, kappaMaxModSliceC):
        #  # Crude Monte-Carlo estimate of tile area.
        #  ntrials=1000000
        #  for i in range(1,ntrials):
        #    ry = random()
        #    ynow = yL + ry*deltaYg
        #    kMax= self.kappaMaxMod(ynow, kappaMax, delta)
        #    rk = random()
        #    know = kappaLowerEdge + rk*2.*deltaYg
        #    if le( know, kMax):
        #      area += 1.0
        #  area = area/(ntrials+0.0)*deltaYg*2.*deltaYg

          #print (y_bin, kappaCenter, kappaMax, delta, "old area=",area, "new area=",area2, kappaLowerEdge, kappaLowerEdge + 2.*deltaYg, kappaMaxModSliceL, kappaMaxModSliceC, kappaMaxModSliceR, " diff=", abs(kappaMaxModSliceR - kappaMaxModSliceL), "left-lower=", kappaMaxModSliceL-kappaLowerEdge, "right-lower", kappaMaxModSliceR-kappaLowerEdge,  minKmodLR + diff1/deltaYg*deltaYg )

        #print (y_bin, kappaLowerEdge, kappaCenter, kappaMax, delta, "old area=",area, "new area=",area2, kappaMaxModSliceL, kappaMaxModSliceC, kappaMaxModSliceR, " diff1=", diff1, "diff2=", diff2, "diff3=", diff3)

        return max(area2,0.0)

    def kappaMaxNow (self, y, kappaMax):
        slope = 1.
        if gt(y, 0.):
          slope = -1.
        kMax = slope*(y-self.y0)*(self.kappaMin-kappaMax)/(self.yMin-self.y0) + kappaMax;
        return kMax

    def kappaMaxMod (self, y, kappaMax, delta):
        slope = 1.
        if gt(y, 0.):
          slope = -1.
        kMax = slope*(y-self.y0)*(self.kappaMin-kappaMax)/(self.yMin-self.y0) + kappaMax - 2.*delta/2.;
        return kMax
      

    def findParticle(self,particle_pos):
        if abs(particle_pos)==1:
          return 0
        if particle_pos==2:
          return len(self.nodes)-1
        inode=-1
        for i in range(len(self.nodes)):
          if self.nodes[i].particle_pos == particle_pos:
            inode=i
            break
        if inode<0:
          print ("particle ", particle_pos, " not found!")
          return -1
        return inode


    def generalized_rapidity(self,start_particle, stop_particle):
        rap = 0.0
        start=self.findParticle(-start_particle)
        stop= self.findParticle(stop_particle)
        rap = self.shortestDistance(start,stop)
        return rap


    def shortestDistance(self,start_node,stop_node):
        istart=start_node
        istop=stop_node
        a=self.nodes[istart].lineColor
        b=self.nodes[istop].lineColor
        l_a = int(m.log10(a))
        l_b = int(m.log10(b))
        l_max=max(l_a,l_b)

        length=0.0
        for i in range(len(self.nodes)):

          inow=self.nodes[i].lineColor
          # skip leading/trailing elements
          if i < istart or i > istop:
            continue
          # skip higher-order folds.
          l_now = int(m.log10(inow))
          if (l_now>l_max):
            continue
          # skip early branches. note: this only works for (1,2,3,2,1) background triangle structure
          skip = False
          if i != istart and i != istop and inow != a and inow != b and inow//100>0 and (inow//100)%10 != (max(a,b)//100)%10:
            skip = True
          if i != istart and i != istop and inow != a and inow != b and inow//10000>0 and (inow//10000) != (max(a,b)//10000):
            skip = True
          if skip:
            continue

          # increase distance
          length += self.nodes[i].real_length()

        return length

    def setup(self):
        self.setup_nodes(True)
        self.setup_split_systems()
        self.store_node_kappa_values()
        self.set_split_systems_kappa_values()
        return None


    def setup_nodes(self, store):
        # Set the correct length of the first and last intervals
        self.nodes[0].length = (self.nodes[0].bkgrnd_ybin+0.5)*deltaYg - self.yMin
        self.nodes[-1].length = self.yMax - (self.nodes[-1].bkgrnd_ybin-0.5)*deltaYg
        # set particle counters
        self.nodes[0].store_particle_pos(1)
        self.nodes[-1].store_particle_pos(2)
        npart=2
        for i in range(1,len(self.nodes)-1):
          if self.nodes[i].stopIsGluon:
            npart+=1
            if store:
              self.nodes[i].store_particle_pos(npart)
            else:
              self.nodes[i].particle_pos = npart
          elif self.nodes[i].startIsGluon:
            if store:
              self.nodes[i].store_particle_pos(-npart)
            else:
              self.nodes[i].particle_pos = -npart
          elif not self.nodes[i].startIsGluon and not self.nodes[i].stopIsGluon:
            if store:
              self.nodes[i].store_particle_pos(0)
            else:
              self.nodes[i].particle_pos = 0
        return None


    def setup_split_systems(self):
        self.split_systems = []
        npart=2
        for i in range(1,len(self.nodes)-1):
          if self.nodes[i].stopIsGluon:
            npart+=1
            self.split_systems.append([0,npart,0, 0.])
        # set neighboring particles to the left
        left_neighbor=1
        for s in self.split_systems:
          s[0] = left_neighbor
          s[2] = s[1]+1
          left_neighbor = s[1]
        if len(self.split_systems)>0:
          self.split_systems[-1][2] = 2
        return None


    def store_node_kappa_values(self):
        # set kappa height for final state particles
        for i in range(len(self.nodes)):
          if self.nodes[i].particle_pos == 0:
            continue
          height = self.calc_kappaHeight(i)

          self.nodes[i].kappa = height * 2.* deltaYg
        # set kappaMax for nodes
        for i in range(len(self.nodes)):
          if self.nodes[i].lineColor < 10000:
            self.nodes[i].store_kappa_max(self.kappaMax)
          # do not set kappaMax for secondary folds (since the kappa max will 
          # change upon smearing the kinematics of the hardest gluons...)
          else:
            self.nodes[i].store_kappa_max(-1.)
            continue
        return None


    def set_split_systems_kappa_values(self):
        for s in self.split_systems:
          i_n = self.node_of_particle(s[1])
          s[3] = self.nodes[i_n].real_kappa()
        return None


    def set_offsets(self, iend, ibeg, delta_k, delta_y):

        # stretch the length of smeared links in kappa-direction
        self.nodes[ibeg].kappa_offset += delta_k
        self.nodes[iend].kappa_offset += delta_k

        # now re-adjust the lengths of the links neighboring branch points that
        # led to i2 and i1, in y-direction
        lc = self.nodes[iend].lineColor
        ileft=-2
        for i_n in range(1,len(self.nodes)):
          if self.nodes[i_n].lineColor == lc:
            ileft=i_n-1
            break
        iright=-2
        for i_n in range(len(self.nodes)-1,1,-1):
          if self.nodes[i_n].lineColor == lc:
            iright=i_n+1
            break
        if ileft>=0 and iright>0:
          self.nodes[ileft].y_offset += delta_y
          self.nodes[iright].y_offset += -delta_y
        else:
          print ("Error: Did not find yshift-able links!", ileft, iright)
          sys.exit()

        return None

    def smear(self, kappa_before):

      if eq(kappa_before, self.kappaOfHighestBkgrndTile):
        return True
      if eq(kappa_before, self.kappaOfHighestBkgrndTile - 2.*deltaYg):
        return True

      return False

    def smear_particles(self):

        deactivate_secondaries = False
        # set the kappa value of the splitting system
        for s in self.split_systems:
          i_stop  = self.node_of_particle(s[1])
          i_start = self.node_of_particle(-s[1])

          if i_start ==-2 or i_stop==-2:
            continue

          s[3] = self.nodes[i_stop].real_kappa()

          if self.smear(s[3]) and self.nodes[i_stop].lineColor < 10000:

            # get smeared rapidity value
            ry = random()
            #igluon = s[1]-3
            #ry = self.randomState.get_next_ry(igluon)
            yMin = self.nodes[i_stop].bkgrnd_ybin*deltaYg - 0.499999*deltaYg
            yMax = self.nodes[i_stop].bkgrnd_ybin*deltaYg + 0.499999*deltaYg
            yNew = yMin + ry*(yMax-yMin)

            # get smeared kappa value
            kappaMin = self.nodes[i_start].kappa - deltaYg
            kappaMax = min(self.nodes[i_start].kappa + deltaYg, self.kappaMaxMod (yNew, self.kappaMax, deltaYq))
            rk = random()
            #rk = self.randomState.get_next_rkappa(igluon)
            kappaNew = kappaMin + rk*(kappaMax-kappaMin)

            deltaKappa = kappaNew - s[3]
            deltaY     = yNew - self.nodes[i_stop].bkgrnd_ybin*deltaYg 

            s[3] = kappaNew

            # reset link lengths to smeared value
            self.set_offsets(i_stop,i_start,deltaKappa,deltaY)

            # now find nodes branching off from this branch, and check if
            # they are (still) viable.
            lc = self.nodes[i_start].lineColor
            parent_index = (lc//100)%10
            for i_n in range(1,len(self.nodes)-1):
              node = self.nodes[i_n]
              particle_pos = node.particle_pos
              isSecondaryBranch = node.lineColor > 10000
              isSecondaryNobranch = node.lineColor > 100 and node.lineColor < 10000 and not node.stopIsGluon and not node.startIsGluon and self.nodes[i_n-1].lineColor < 10000 and self.nodes[i_n+1].lineColor < 10000

              if not isSecondaryBranch and not isSecondaryNobranch:
                continue
              if isSecondaryBranch and (node.lineColor//100)%10 != parent_index:
                continue

              if lt(kappaNew, self.kappaMinSecondary):
                if isSecondaryBranch:
                  deactivate_secondaries = True
                  node.startIsGluon = False
                  node.stopIsGluon = False
                  node.particle_pos = 0
                  node.y_offset = 0.0
                  node.kappa_offset = -2.*node.length
              else:
                # calculate area of all tiles to calculate relative probability
                # only works for secondary folds on [1,2,3,2,1] structure
                max_height = 2
                ybin = 1.0

                sum_real_area = 0.;
                real_areas = []
                for h in range(0,max_height):
                  area = self.calcPhasespaceArea(ybin, 2.*h*deltaYg , deltaYg, kappaNew)
                  sum_real_area += area
                  real_areas.append(area)
                real_prob = []
                for h in range(0,max_height):
                  if gt(real_areas[h], 0.):
                    real_prob.append( real_areas[h] / sum_real_area)

                node_height = self.calc_kappaHeight(i_n)

                # now calculate probability
                new_weight = 1.0
                if len(real_prob)>0 and node_height < len(real_prob):
                  new_weight = (real_prob[node_height] / 0.5)

                if isSecondaryNobranch or (isSecondaryBranch and node.stopIsGluon):
                  self.smearing_weight *= new_weight

        if deactivate_secondaries:
          self.setup_nodes(False)
          self.setup_split_systems()
          self.set_split_systems_kappa_values()

        return None

    def node_of_particle(self, particle_pos):
        i_node=-2
        for i in range(len(self.nodes)):
          if self.nodes[i].particle_pos == particle_pos:
            i_node=i
            break
        return i_node


    def reset(self):
        self.smearing_weight = 1.0
        for n in self.nodes:
          n.reset()
        self.split_systems = []
        self.setup_split_systems()
        self.set_split_systems_kappa_values()
        for s in self.split_systems:
          s[3] = 0.0
        return None

