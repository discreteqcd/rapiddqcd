
# system imports
from __future__ import print_function
import sys, optparse

nevents=100000

sys.path.append('utils')

from basics import le
from random import random

def discrete_inverse_trans(prob_vec):
    U=random()
    if le(U,prob_vec[0]):
      return 0
    else:
      for i in range(1,len(prob_vec)+1):
        if sum(prob_vec[0:i])<U and sum(prob_vec[0:i+1])>U:
          return i

# math imports
from math import log as ln

# hard ME imports
from matrix import eetojj
from LHEF3 import *

# basic shower data structures
from primitive import Primitive
from primitive_kinematics import Kinematics
from primitive_scales import LambdaQCD, LambdaCutOff

# Calculate properties of phase space triangle
ecm=91.2
kappaMin=ln(LambdaCutOff*LambdaCutOff/(LambdaQCD*LambdaQCD))
kappaMax=ln(ecm*ecm/(LambdaQCD*LambdaQCD))
yMin=-kappaMax/2.
yMax=kappaMax/2.

# init event generation
hardprocess = eetojj(ecm)

# LHEF Initialization
lhe = LHEF("qsim-for-ref-report-0dot5.lhe")
initrwgt = LHAinitrwgt()
wt = LHAweight()
wt.id = "central"
wt.text = " mur=" + str(0.5*ecm) + " muf=" + str(0.5*ecm)
initrwgt.weights[wt.id] = wt
lhe.SetHeader(3.0,initrwgt)
init = LHAinit()
init.beams = [11,-11]
init.energy = [0.5*ecm,0.5*ecm]
init.pdfgroup = [-1,-1]
init.pdfset = [-1,-1]
init.wgtid = -4
xsec = 0
xerr = 0

# Construct all primitive structures [A..X]
print ("prepare all primitive structures, ", LambdaQCD)
p = []
probVec = []
weightVec = []
name_vec = []
kmin=-1.
for c in list(map(chr,range(ord('A'),ord('X')+1))):
  name_vec.append(c)
  p.append(Primitive(c, yMin, yMax, kappaMin, kappaMax))
  if len(p)==1:
    kmin = p[-1].calc_kappa_min_secondary()
  else:
    p[-1].set_kappa_min_secondary(kmin)

  probVec.append(p[-1].prob)
  weightVec.append(p[-1].weight)

for iev in range(0,nevents):

  if iev%10000==0:
    print ("\n\n|------new event: #", iev,"-------------|")

  index = discrete_inverse_trans(probVec)

  p[index].smear_particles()
  event, weight = hardprocess.GeneratePoint()

  if event[2].mom.Y() < 0.:
    event[2].SetPos(1)
    event[3].SetPos(2)
  else:
    event[2].SetPos(2)
    event[3].SetPos(1)

  kinematics = Kinematics(p[index])
  kinematics.createEvolvedEvent(event,2.*yMax)

  weight *= p[index].event_weight()
  xsec += weight
  xerr += weight*weight

  lhe.CreateEvent(event,weight)
  # clean up
  p[index].reset()

process = LHAProcess(xsec,xerr,xsec,9999)
init.processes.append(process)
lhe.SetInitBlock(init)
lhe.AddAllEvents()
lhe.Write()

