# Rapid Discrete QCD: a fast implementation of discretized particle evolution

Welcome to the repository of the Rapid DQCD parton shower, which was introduced as a classical baseline for the [quantum algorithm](https://arxiv.org/abs/2207.10694). This implementation is based on generating and caching the "groves" of the DQCD upon initialization -- making the event generation lightning fast.

## Requirements 

The code is written in Python 3.

## Running the code.

We'll add the code to the repository as soon as the dust settles on the quantum article.

## Authors and acknowledgment

The ideas for the algorithm were developed by Simon Williams and Stefan during discussions about the limitations of current quantum hardware.
The implementation is due to Stefan Prestel -- feel free to get in touch! 

## License

This software is licenced under the GNU GPL v2 or later.

